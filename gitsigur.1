'\" t macro stdmacro

.de SAMPLE
.br
.RS 0
.nf
.nh
..
.de ESAMPLE
.hy
.fi
.RE
..

.TH "GITSIGUR" "7"

.SH "NAME"
gitsigur \- authorization/authentication for gpg-/ssh-signed git repos

.SH "SYNOPSYS"
.B gitsigur [\fIOPTION\fP]... REF OLD NEW

.SH "DESCRIPTION"

\fBgitsigur\fP is a tool for authenticating and authorizing gpg- and ssh-
signed commits to a git repository with shared access.

\fBgitsigur\fP may be directly installed as an "update" hook of the
git repo being protected.  It produces a brief report on stdout and
signals success or failure by exit status.  \fBgitsigur\fP may also be
run by hand for offline verification.  It must be launched under the
git repo being protected.

If REF is matches a list of configured patterns, gitsigur checks each commit
in the range OLD..NEW (see \fBgitversions\fP(7)) for being gpg signed.
Further, it checks that each signature is by gpg keys designated for
use by the particular git committer.  This is intended to improve
"source code supply chain security" by defeating impersonation, along
with signed commit assurance that the code is not tampered-with.

The key designation information is carried in a branch of some local
git repository ("key git repo") specified by configuration.  Given a
committer email address, all public keys in the key git repo, matching
"$email/*.asc" "$email/*.gpg" (for gpg) "$email/*.pub" (for ssh).
These are used to construct temporary keyrings.  For example, if a
committer was "Foo Bar <jane@doe.com>", then in the key git repo's
given branch, files named
.SAMPLE
    /jane@doe.com/key1.gpg
    /jane@doe.com/key2.asc
    /jane@doe.com/key3.pub
.ESAMPLE
will be included.  The temporary keyrings are in turn used to verify the
git commit.  If the keys have validity problems such as expiry, the
gpg import may fail.

Logistics of the management of this key git repository is outside the
scope of gitsigur (for now).  The key git repo should be public, so
others can also use it to verify commits manually, but be carefully
access controlled with respect to modification.  It is fine to share
the same key git repo & branch for multiple distinct projects, if the
developer pool is about the same.


.SS CONFIGURATION

Configuration options may be stored in the \fBgit-config\fP section
\fB[gitsigur]\fP of the \fB$GIT_DIR\fP where gitsigur was
invoked.  They may also be given as command line options (which
override \fBgit config\fP).

.TP
.B "\-h" "\-\-help"
Dangerous option, use sparingly.

.TP
.B "\-\-loglevel \fILOGLEVEL\fP"   
.TQ
.B "git config gitsigur.loglevel \fILOGLEVEL\fP"
Set the diagnostic log level for the process.  The available modes
include DEBUG, INFO, WARNING, ERROR, CRITICAL.
The default is "critical".

.TP
.B "\-\-mode \fIMODE\fP"
.TQ
.B "git config gitsigur.mode \fIMODE\fP"
Set the operation mode for strictness.  In "enforcing" mode, every
commit must be signed by an appropriate key, and no other error must
occur.  In "permissive" mode, every commit is still judged, but a
success result code is returned for most problems, including
bad or missing signatures.  The default is "permissive".

.TP
.B "\-\-checkref \fICHECKREF\fP"
.TQ
.B "git config --add gitsigur.checkref \fICHECKREF\fP"
Add the given parameter to the list of glob (fnmatch) patterns.  The
command line \fBREF\fP (tag or branch name) given to gitsigur must
match at least one of these patterns to enable gitsigur protection.
The intent is that in a shared repo, only some branches may benefit
from signature enforcement (main shared development branch), and not
others (personal scratch/topic branches).  This option may be
repeated.  The default is to check every ref (tag or branch).

.TP
.B "\-\-nocheckref \fINOCHECKREF\fP"
.TQ
.B "git config --add gitsigur.nocheckref \fINOCHECKREF\fP"
Add the given parameter to the list of glob (fnmatch) patterns to exclude
from checks.  A pattern that matches both \-\-checkref and \-\-nocheckref
is \fBexcluded\fP.
The default is to exclude nothing.

.TP
.B "\-\-elide \fINUM\fP"
.TQ
.B "git config --add gitsigur.elide \fINUM\fP"
Beyond NUM commits in this batch of updates, print a summary of signed/unsigned
commit counts, rather than a line for each of them.  This avoids seeing very
long reports about ancient commits in the case of pushing to new branches.
The default is 5.

.TP
.B "\-\-keygitrepo \fIKEYGITREPO\fP"
.TQ
.B "git config gitsigur.keygitrepo \fIKEYGITREPO\fP"
Set the given path as the key git repo.  It should be a local working
tree or bare repo.  There is no default, as configuration is
mandatory.

.TP
.B "\-\-keygitbranch \fIKEYGITBRANCH\f"
.TQ
.B "git config gitsigur.keygitbranch \fIKEYGITBRANCH\fP"
Set the branch name in the key git repo to search for committer gpg/ssh
keys.  The default is "keymaster".


.SH "MANUAL OPERATION"

In addition to drop-in usage as a git "update" hook, gitsigur may also
be run manually to verify commit signatures of a repository.  The
following example checks the last couple of commits in gitsigur's own
repository.

.SAMPLE
% git clone https://sourceware.org/git/keygitrepo.git
% git clone https://sourceware.org/git/gitsigur.git
% cd gitsigur
% gitsigur --keygitrepo=../keygitrepo master HEAD^^^^ HEAD
or
% git config gitsigur.keygitrepo `pwd`/../keygitrepo
% gitsigur master HEAD^^^^ HEAD
.ESAMPLE

.SH "EASIER MANUAL OPERATION"

If the above sounds silly, try \fBgitsigur-export\fP instead.  It
extracts all the signing keys from a keygitrepo, letting you import
them into your normal environment.  Then you can use normal git
commands to verify signatures.


.SH "EXIT STATUS"

In certain critical configuration errors, the exit status is non-zero.  Otherwise:

In enforcing mode, only if all commits in range are signed by
designated keys, the exit status is 0.  In permissive mode, the exit
status is 0 even in the face of errors.


.SH "COPYRIGHT"

Copyright \(co 2023 Red Hat, Inc.
SPDX-License-Identifier: GPL-3.0-or-later

.SH "SEE ALSO"
.PP
\fBgitsigur-export\fP(1),
\fBgit\fP(1),
\fBgithooks\fP(1),
\fBgitversions\fP(7),
\fBgpg\fP(1),
\fBssh\fP(1),
\fBfnmatch\fP(3)

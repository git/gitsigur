#! /usr/bin/python3

# Copyright (C) 2023-2025 Red Hat
# SPDX-License-Identifier: GPL-3.0-or-later

import git
import subprocess
import argparse
import logging
import os
import sys
import tempfile
import fnmatch
import glob
from typing import Dict


# globals
args = None
protectedrepo = None
keygitrepo = None


def main():
    try:
        global protectedrepo
        protectedrepo = git.Repo(".") # Require start from a valid . = $GIT_DIR
    except Exception as e:
        logging.critical(f"gitsigur must be started in protected git repo")
        return 1

    # NB: we must clear this environment variable that is arriving via
    # githooks, since we're going to be using two separate repos via
    # gitpython and don't want this var interfering.
    os.environ.pop('GIT_DIR', None)
    
    prconfig = protectedrepo.config_reader()
    
    parser = argparse.ArgumentParser(description='Verify git commit signatures.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)


    parser.add_argument('--loglevel',type=str,help='logging level',default="critical",
                        choices=["critical","error","warning","info","debug"])
    parser.add_argument('--mode',type=str,help='operating mode',choices=["enforcing","permissive"],
                        default="permissive")
    parser.add_argument('--elide',type=int,help='hide signature details after this many commits',default=5)
    parser.add_argument('--checkref',type=str,action='append',help='may only check refspecs matching given glob',default=[])
    parser.add_argument('--nocheckref',type=str,action='append',help="must not check refspecs matching given glob",default=[])
    parser.add_argument('--keygitrepo',type=str,help='git repo containing public keys',default=".")
    parser.add_argument('--keygitbranch',type=str,help='branch in keygitrepo containing committer pubkeys',
                        default="keymaster")

    parser.add_argument("REF",type=str,help="name of the ref being updated")
    parser.add_argument("OLD",type=str,help="old object name stored in the ref")
    parser.add_argument("NEW",type=str,help="new object name stored in the ref")
    

    # parse argv and corresponding git-config parameters
    global args

    def prconfig_get(name):
        section = "gitsigur"
        a = []
        if prconfig.has_section(section):
            for (k,values) in prconfig.items_all(section):
                if k == name:
                    for value in values:
                         a.extend(["--"+name, value])
        return a

    argv = (prconfig_get("loglevel") +
            prconfig_get("mode") +
            prconfig_get("keygitrepo") +
            prconfig_get("keygitbranch") +
            prconfig_get("checkref") +
            prconfig_get("nocheckref") +
            prconfig_get("elide") +            
            sys.argv[1:])

    args = parser.parse_args(argv)
    
    logging.basicConfig(level=args.loglevel.upper(),
                        format="%(asctime)s:"+os.path.basename(__file__)+":%(levelname)s:%(message)s")
    logging.captureWarnings(True)

    logging.debug(f"command line args {args}")
    
    # check whether this ref needs to be processed at all
    if len(args.checkref) == 0:
        check = True # empty --checkref implies default-true
    else:
        check = False
    for glob in args.checkref:
        if fnmatch.fnmatch(args.REF, glob):
            logging.debug(f"ref {args.REF} matches checkref {glob}")
            check=True
    for glob in args.nocheckref:
        if fnmatch.fnmatch(args.REF, glob):
            logging.debug(f"ref {args.REF} matches nocheckref {glob}")
            check=False
                
    if not check:
        return 0 # quiet success

    # check that key git repo is available
    if not args.keygitrepo:
        logging.critical(f"missing keygitrepo configuration")
        return 1
    global keygitrepo
    keygitrepo = git.Repo(args.keygitrepo)

    if args.NEW == "0000000000000000000000000000000000000000": # ref being removed
        return 0 # nothing to check

    print(f"gitsigur checking ({args.mode}) against keygitrepo {args.keygitrepo} branch {args.keygitbranch}")
                
    unsigned = []
    signed = []
    for commit in protectedrepo.iter_commits(
            (args.NEW) if args.OLD == "0000000000000000000000000000000000000000" else (args.OLD + ".." + args.NEW)):

        logging.debug(f"commit {commit.hexsha} has-gpgsig {commit.gpgsig != ''}")
        if not commit.gpgsig:
            unsigned.append(commit)
            if len(signed)+len(unsigned) <= args.elide: # nb: len(list) is O(1)
                print(f"\u2A2F commit {commit.hexsha} not signed")
        else:
            try:
                check_commit(commit)
                signed.append(commit)
                if len(signed)+len(unsigned) <= args.elide:                
                    print(f"\u2713 commit {commit.hexsha} signed by authorized key for {commit.committer.email}")
            except Exception as e:
                unsigned.append(commit)
                if len(signed)+len(unsigned) <= args.elide:
                    print(f"\u2A2F commit {commit.hexsha} not signed by authorized key for {commit.committer.email}")
                logging.exception(e)

    # summarize if needed
    if len(signed)+len(unsigned) > args.elide:
        if len(signed) > 0:
            print(f"... \u2713 total {len(signed)} commit(s) signed")
        if len(unsigned) > 0:
            print(f"... \u2A2F total {len(unsigned)} commit(s) not signed")        
            
    logging.debug(f"number of unsigned commits: {len(unsigned)}, signed commits: {len(signed)}")

    if args.mode == "enforcing" and len(unsigned) > 0:
        print(f"gitsigur result: \u2A2F failure")
        return 1
    else:
        print(f"gitsigur result: \u2713 success")
        return 0 # success


gpghomes : Dict[str, tempfile.TemporaryDirectory] = dict()  # committer email to TemporaryDirectory(), auto-delete on program exit
def find_gpghome(committer):
    """Find or create a gpg/ssh homedir for given committer.  It should include
    only this person's approved signing keys."""

    # memoized!
    global gpghomes
    if committer in gpghomes:
        return gpghomes[committer].name

    dirname = tempfile.TemporaryDirectory()

    # extract given committer's public keys from the keygitrepo branch
    keygittree = keygitrepo.heads[args.keygitbranch].commit.tree
    keygitsubtree = keygittree[committer]  # may throw if committer is unknown

    for keyblob in keygitsubtree:
        extension = os.path.splitext(keyblob.path)[-1]
        # accumulate gpg keys in a temp keyring
        if extension in [".gpg", ".asc"]: # gpg: add to a gpg keyring
            with tempfile.NamedTemporaryFile(delete=False) as keyfile:
                keyfile.write(keyblob.data_stream.read())
                keyfile.flush() # NB: flush the output to disk so gpg can read it
                rc = subprocess.run(["gpg", "--import", keyfile.name],
                                    cwd=dirname.name,
                                    env={"GNUPGHOME":dirname.name},
                                    encoding="utf-8",
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)

                logging.debug(f"gpg --import {keyblob.name}: rc {rc.returncode} stdout {rc.stdout} stderr {rc.stderr}")
                rc.check_returncode()
        # accumulate ssh keys in a temp allowed_signers file
        elif extension in [".pub"]: # ssh: add it to the allowed_signers file
            with open(os.path.join(dirname.name, "allowed_signers"),"at") as pubfile:
                pubfile.write(committer + ' ' + keyblob.data_stream.read().decode('utf-8'))
            logging.debug(f"allowed_signer {keyblob.name}")
        else:
            logging.debug(f"ignored keygit file {keyblob.path} with unknown extension {extension}")

    logging.debug(f"created gpgtempdir {dirname}")
    gpghomes[committer] = dirname
    return dirname.name
    
            
def check_commit(commit):
    """Check authorization & authentication of given commit in protectedrepo.
    Throw an exception on errors.
    """
    committer = commit.committer.email
    logging.debug(f"processing commit {commit.hexsha} by {committer}")
    gpghomedir = find_gpghome(committer)

    if "BEGIN PGP SIGNATURE" in commit.gpgsig:
        # run git to verify against committer gpg keyring
        rc = subprocess.run(["git", "verify-commit", "--raw", commit.hexsha],
                            env={"GNUPGHOME": gpghomedir},
                            encoding="utf-8",
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

        logging.debug(f"git verify-commit: rc {rc.returncode} stdout {rc.stdout} stderr {rc.stderr}")
        rc.check_returncode()
    elif "BEGIN SSH SIGNATURE" in commit.gpgsig:
        # Split the commit object text into the signature header (contents), and
        # everything else.  See also git commit.c remove_signature().
        with tempfile.NamedTemporaryFile(mode='w',encoding='utf-8') as sigfile:
            with tempfile.NamedTemporaryFile(mode='w',encoding='utf-8') as commitfile:
                state = 'header'
                # NB: the [0:-1] bit trims the '' split() result following the final \n
                for line in commit.data_stream.read().decode('utf-8').split('\n')[0:-1]:
                    if state == 'header' and line.startswith('gpgsig'):
                        sigfile.write(' '.join(line.split(' ')[1:])+'\n')
                        state='gpgsig'
                    elif state == 'header' and line == '': # empty line
                        commitfile.write(line+'\n')
                        state = 'message'
                    elif state == 'header': # any other header
                        commitfile.write(line+'\n')
                    elif state == 'gpgsig' and line.startswith(' '): # multiline extension header
                        sigfile.write(' '.join(line.split(' ')[1:])+'\n')
                    elif state == 'gpgsig' and line == '': # empty line
                        commitfile.write('\n')
                        state = 'message'
                    elif state == 'gpgsig': # some other header continuing
                        commitfile.write(line+'\n')
                        state = 'header'
                    elif state == 'message':
                        commitfile.write(line+'\n')
                        # terminal state
                sigfile.flush()
                commitfile.flush() # so ssh verification commands can read these files

                commitfile.seek(0) # prep to read
                rc = subprocess.run(["ssh-keygen", "-v", "-v", "-v",
                                     "-Y", "verify", "-n", "git",
                                     "-I", committer,
                                     "-f", os.path.join(gpghomedir, "allowed_signers"),
                                     "-s", sigfile.name],
                                    stdin=commitfile,
                                    encoding="utf-8",
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
                logging.debug(f"ssh -Y verify: rc {rc.returncode} stdout {rc.stdout} stderr {rc.stderr}")
                if rc.returncode == 0:
                    return
        raise RuntimeError("no matching ssh key found")
    else:
        raise RuntimeError("Unknown gpgsig style " + (commit.gpgsig.split('\n')[0]))

if __name__ == '__main__':
    sys.exit(main())
                  

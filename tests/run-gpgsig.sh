#! /bin/bash

type gpg || exit 77
type git || exit 77

set -e
set -x
trap 'rm -rf $tmpdirs' 0 ERR

# env
builddir=`pwd`
srcdir=`cd $srcdir; pwd` # absolutize
# prefix=?

GPG=`pwd`/gpghome-$$
mkdir $GPG
chmod og-rwx $GPG
tmpdirs="$tmpdirs $GPG"

export GNUPGHOME=$GPG
gpg --import $srcdir/gpgkeys/privkey.gpg
# echo -e "trust\n5\ny\n" | gpg --command-fd 0 --edit-key 06BDF006A9EDE00F

# make source repo
GIT=`pwd`/githome-$$
tmpdirs="$tmpdirs $GIT"
git init $GIT
(cd $GIT; git config user.signingkey 06BDF006A9EDE00F)
(cd $GIT; git config user.name Gitsigur)
(cd $GIT; git config user.email gitsigur@sourceware.org)
(cd $GIT; git config committer.name Gitsigur)
(cd $GIT; git config committer.email gitsigur@sourceware.org)

# make a gitsigur key repo
GIT2=`pwd`/gitkeyhome-$$
tmpdirs="$tmpdirs $GIT2"
git init $GIT2
(cd $GIT2; git config user.name Gitsigur)
(cd $GIT2; git config user.email gitsigur@sourceware.org)
(cd $GIT2; git config committer.name Gitsigur)
(cd $GIT2; git config committer.email gitsigur@sourceware.org)

# make a gpg-signed commit
(cd $GIT; echo hello world > file)
(cd $GIT; git checkout --orphan master; git add file; git commit -mtest -S file)

# check that git verifies the signature 
(cd $GIT; git log --show-signature -p)
commit=$(cd $GIT; git log --no-show-signature --format=oneline | awk '{print $1}')
zeroes=0000000000000000000000000000000000000000

# run gitsigur; should pass but warn without keygitrepo
(cd $GIT; $srcdir/../gitsigur --mode=permissive master $zeroes $commit) | grep 'gitsigur result:.*success'
(cd $GIT; $srcdir/../gitsigur --mode=enforcing master $zeroes $commit || true) | grep 'gitsigur.result.*failure'

# create keygitrepo
mkdir $GIT2/gitsigur@sourceware.org
cp -p $srcdir/gpgkeys/pubkey.gpg $GIT2/gitsigur@sourceware.org
(cd $GIT2; git checkout --orphan keymaster; git add .; git commit -minitialize)

# try again
(cd $GIT; $srcdir/../gitsigur --loglevel=debug --keygitrepo=$GIT2 --mode=enforcing master $zeroes $commit) | grep 'gitsigur.result.*success'

# add a new commit without signature
(cd $GIT; echo hello world > file2)
(cd $GIT; git add file2; git commit -mtest file2)
(cd $GIT; git log)
commit2=$(cd $GIT; git log --no-show-signature --format=oneline | head -1 | awk '{print $1}')

# it must be rejected (even though first commit still has signature)
(cd $GIT; $srcdir/../gitsigur --loglevel=debug --keygitrepo=$GIT2 --mode=enforcing master $zeroes $commit2 || true) | grep 'gitsigur.result.*failure'

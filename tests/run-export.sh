#! /bin/bash

type gpg || exit 77
type git || exit 77

set -e
set -x
trap 'rm -rf $tmpdirs' 0 ERR

# env
builddir=`pwd`
srcdir=`cd $srcdir; pwd` # absolutize
# prefix=?

# make a gitsigur key repo
GIT2=`pwd`/gitkeyhome-$$
tmpdirs="$tmpdirs $GIT2"
git init $GIT2
(cd $GIT2; git config user.name Gitsigur)
(cd $GIT2; git config user.email gitsigur@sourceware.org)
(cd $GIT2; git config committer.name Gitsigur)
(cd $GIT2; git config committer.email gitsigur@sourceware.org)

# create keygitrepo
mkdir $GIT2/foo@example.com
cp -p $srcdir/sshkeys/id-rsa.pub $GIT2/foo@example.com
(cd $GIT2; git checkout --orphan keymaster; git add .; git commit -minitialize)

mkdir $GIT2/gitsigur@sourceware.org
cp -p $srcdir/gpgkeys/pubkey.gpg $GIT2/gitsigur@sourceware.org
(cd $GIT2; git add .; git commit -manother-one)

cp -p $srcdir/sshkeys/id-ed25519.pub $GIT2/foo@example.com
echo 'hello world' > $GIT2/foo@example.com/noise.txt
(cd $GIT2; git add .; git rm -f foo@example.com/id-rsa.pub; git commit -manother-two)


# extract just last-state keys
$srcdir/../gitsigur-export --loglevel=debug --keygitrepo $GIT2 --keygitbranch keymaster

tmpdirs="$tmpdirs gs_pubring.asc gs_allowed_signers"
ls -ald gs_pubring.asc
ls -ald gs_allowed_signers

GPG1=`pwd`/gpg1$$
mkdir -p $GPG1
tmpdirs="$tmpdirs $GPG1"
gpg --homedir $GPG1 --import gs_pubring.asc
gpg --homedir $GPG1 --list-keys | grep A9EDE00F

if ! grep foo@example.com.ssh-rsa < gs_allowed_signers; then
    true
else
    false # this file was git rm -f'd in the last commit so this key shouldn't show up
fi
cat gs_allowed_signers | grep foo@example.com.ssh-ed25519

rm -f gs_pubring.gpg gs_allowed_signers

# extract keys going all the way back in history
$srcdir/../gitsigur-export --all --loglevel=debug --keygitrepo $GIT2 --keygitbranch keymaster

ls -ald gs_pubring.asc
ls -ald gs_allowed_signers

GPG2=`pwd`/gpg2$$
mkdir -p $GPG2
tmpdirs="$tmpdirs $GPG2"
gpg --homedir $GPG2 --import gs_pubring.asc
gpg --homedir $GPG2 --list-keys | grep A9EDE00F

test 1 == `cat gs_allowed_signers | grep foo@example.com.ssh-rsa | wc -l` # no duplicates
test 1 == `cat gs_allowed_signers | grep foo@example.com.ssh-ed25519 | wc -l` # no duplicates


